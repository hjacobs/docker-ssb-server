FROM node:14

RUN npm install -g ssb-server

EXPOSE 8008

HEALTHCHECK --interval=30s --timeout=30s --start-period=10s --retries=10 \
  CMD ssb-server whoami || exit 1
ENV HEALING_ACTION RESTART

ENTRYPOINT [ "/usr/local/bin/ssb-server" ]
CMD [ "start" ]
