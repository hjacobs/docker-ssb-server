# Dockerized ssb-server

This repository contains a few scripts to run `ssb-server` as a Docker container.

Instructions were taken from https://willschenk.com/articles/2020/setting_up_ssb_pub_server/

## Instructions

```
docker build -t ssb-server .
mkdir ~/pubdata
docker run -d --init --name ssb-server -v ~/pubdata/:/root/.ssb/ --net host --restart unless-stopped ssb-server
```

```
./ssb-server whoami
```

```
./ssb-server invite.create 3
```

